<html>
	<head>
        <link rel="stylesheet" href="style.css" type="text/css">
	</head>
	<body>
        <div class="total_form">
    		<form action="repeat.php" method="GET">
			     <center><h1>Appendix A-Sample US Death Certificate Form</h1></center>
                    <p>The sample death reporting form including in this content profile reflects much of the data captured                        for the U.S. Standard Certificate of death. However, the VRDR Content profile may be modified to                            include and accommodate international death reporting requirements.</p>
			     <center><h1>Death Reporting for vital Records</h1></center>
		          <h2>Decendent's Name (include AKA's if any)</h2>
		          <form action="death_form" method="POST">

			     <fieldset>
                    <div class="decendent_names">
                        <div class="decendent_names_form_line_1">
                            <div class="f_1_l1">Last Name <br> <input style="height:30px; border:1px solid #cecece;" type="last_name" size="45"></div>
                            <div class="f_2_l1">First Name  <br> <input style="height:30px; border:1px solid #cecece;" type="first_name" size="45"></div>
                            <div class="f_3_l1">Middle Name  <br> <input style="height:30px; border:1px solid #cecece;" type="middle_name" size="45"></div>
                        </div>    
                        <div class="decendent_names_form_line_2">
                            <div class="f_1_l2">Date of birth  <br> <input style="height:30px; border:1px solid #cecece;" type="date_of_birth" size="45"></div>
                            <div class="f_2_l2">Gender  <br> <select name="gender">
                                                <option value="male">Male</option>
                                                <option value="female">female</option>
                                            </select>
                            </div>
                            <div class="f_3_l2">Social Security Number  <br> <input style="height:30px; border:1px solid #cecece;" type="social_security_number" size="23"></div>
                            <div class="f_4_l2">Facility Name  <br> <input style="height:30px; border:1px solid #cecece;" type="facility_name" size="45"></div>
                        </div>    
                     </div>
			     </fieldset>

			<h2>Decendent's Hispanic Origin</h2>

                    <p><b>Check the box that best describes whether the decedent is Spanish / Hispanic / Latino. Check the "No" box if decendent is not Spanish / Hispanic / Latino :</b></p>
                <br>
                      <div class="decendent_hispanic">
                          <div class="decendent_hispanic_form_line_1">
                              <div class="d_h_r_1"><input type="radio" name="hispanic_origin">No, Not Spanish/Hispanic/Latino</div>
                            <div class="d_h_r_1"><input type="radio" name="hispanic_origin">Yes, Mexican, Mexican American, Chicano</div>
                            <div class="d_h_r_1"><input type="radio" name="hispanic_origin">Yes, Puerto Rican</div>
                            <div class="d_h_r_1"><input type="radio" name="hispanic_origin">Yes, Cuban
                          </div>      
                        </div>
                          <div class="decendent_hispanic_form_line_2">
                            <input type="radio" name="hispanic_origin">Yes, Other  Spanish/Hispanic/Latino(Specify):<br>
                            <input style="height:30px; border:1px solid #cecece;" type="text" name="spanish_specify" size="100">
                          </div>
                        </div>
                            <br>
                            <br>
			<h2>Decendent's Hispanic Origin</h2>

			<p><b>Check one or more races to indicate what the Decendent considered herself to be:  </b></p>

				<input type="radio" name="decedent_race">White
				<input type="radio" name="decedent_race">Filipino
				<input type="radio" name="decedent_race">Native Hawaiian
				<input type="radio" name="decedent_race">Black or African American <br>
				<input type="radio" name="decedent_race">Japanese 
				<input type="radio" name="decedent_race">Guamanian or Chamorro
				<input type="radio" name="decedent_race">American Indian or Alaska Native (Name of the enrolled or principal Tribe) <input type="text" name="alaska_native">
				<input type="radio" name="decedent_race">Vietnamese<br>
				<input type="radio" name="decedent_race">Other Pacific Islander (Specify) <input type="text" name="other_pacific">
				<input type="radio" name="decedent_race">Asian Indian
				<input type="radio" name="decedent_race">Other Asian (specify) <input type="text" name="other_asian">
				<input type="radio" name="decedent_race">Other (Specify) <input type="text" name="otehr_specify"> <br>
				<input type="radio" name="decedent_race">Chinese
				<input type="radio" name="decedent_race">Korean
				<input type="radio" name="decedent_race">Samoan

		<h2>Items Must be Completed by person Who Pronounces or Certificate Death.</h2>

				Date Pronounced Dead <br> <input type="text" name="date_pronounced_dead"><br>
				Time Pronounced Dead <br> <input type="text" name="time_pronounced_dead"><br>
				Signature of Pronouncing Death <br> <input type="Signature_of_Pronouncing_Death"><br>
				License Number <br> <input type="license_number"><br>
				Date Signed <br> <input type="date_signed"><br>
				Actual Or Person Pronouncing Death <br> <input type="Actual_Or_Person_Pronouncing_Death"><br>
				Actual Or Presumed time of Death <br> <input type="Actual_Or_Presumed_time_of_Death"><br>
				Was Medical Examiner Or Coroner Contacted <br> <input type="radio" name="Medical_Examiner">Yes <input type="radio" name="Medical_Examiner">No


		<h2>Cause of Death ( See Instructions and examples)</h2>
		<p>PART 1. Enter the chain of events -- diseases, injuries, or complications -- that directly caused the death. DO NOT enter terminal events such as Approxmateinterval : cardiac arrest, respioratory arrest, or ventricular fibrillation without showing  the etiology. DO NOT ABBREVIATE. Enter only one cause ona line.<br> Ass Additional lines if necessary.</p>

			a. 	Immediate Cause (Final diseases or condition resulting in death) <br> <input type="text" name="Immediate_Cause"><br>
				Due to (or as a consequence of): <br> <input type="text" name="Immediate_Cause_Due_to"><br>
				Onset to death: <br> <input type="text" name="Immediate_Cause_Onset_to_death"><br>
			b. 	Sequentially list Conditions, (if any, leading to the cause listed on line a.) <br> <input type="text" name="Sequentially_list_Conditions"><br>
				Due to (or as a consequence of): <br> <input type="text" name="Sequentially_list_Conditions_Due_to"><br>
				Onset to death: <br> <input type="text" name="Sequentially_list_Conditions_Onset_to_death"><br>
			c. 	Enter the Underlying Cause, (Diseases or injury that initiated the events resulting in death)<br><input type="text" name="Enter_the_Underlying_Cause"><br>
				Due to (or as a consequence of): <br> <input type="text" name="Enter_the_Underlying_Cause_Due_to"><br>
				Onset to death: <br> <input type="text" name="Enter_the_Underlying_Cause_Onset_to_death"><br>
			d. 	Last <br><input type="text" name="last_cause_of_death"><br>
				Onset to death: <br> <input type="text" name="Enter_the_Underlying_Cause_Onset_to_death"><br>
 
		<p><b>PART 2.</b> Enter other significent conditions contributing to death but not resulting in the underlying cause given in PART 1. </p>
			<input type="text" name="Enter_other_significent_conditions"><br>
			Was An Autospy performed? <br>
			<input type="radio" name="Autospy_performed">Yes
			<input type="radio" name="Autospy_performed">No
			<br>
			Were Autospy Finding Available to complete the cause of death?
			<input type="radio" name="complete_the_cause_of_death">Yes
			<input type="radio" name="complete_the_cause_of_death">No
			
			<br>
			Did Tobaco use contribute to death?
			<input type="radio" name="complete_the_cause_of_death">Yes
			<input type="radio" name="complete_the_cause_of_death">No
			<input type="radio" name="complete_the_cause_of_death">Probably
			<input type="radio" name="complete_the_cause_of_death">unknown
            
        <h2>If female:</h2>
            <input type="radio" name="if_female">Not Pregnent within past year 
            <input type="radio" name="if_female">Pregnent at time of death 
            <input type="radio" name="if_female">Not Pregnent, but pregnent within 42 days of death
            <input type="radio" name="if_female">Not Pregnent, but pregnent 43 days to 1 year before death 
            <input type="radio" name="if_female">Unknown if pregnent within the past year
            
         <h2>Manner of death</h2> 
            <input type="radio" name="manner_of_death">Natural 
            <input type="radio" name="manner_of_death">Homicide 
            <input type="radio" name="manner_of_death">Accident 
            <input type="radio" name="manner_of_death">Pending Investigation 
            <input type="radio" name="manner_of_death">Sucide 
            <input type="radio" name="manner_of_death">Could not be determined
            
         <h2>Injury</h2>
            Date of Injury <br> <input type="text" name="date_of_injury"><br>            
            Time of Injury <br> <input type="text" name="time_of_injury"><br>            
            Palace of Injury <br> <input type="text" name="palace_of_injury"><br>            
            Injury at work
			<input type="radio" name="injury_at_work">Yes
			<input type="radio" name="injury_at_work">No
            
            
        <h2>Location of Injury</h2>          
            state <br> <input type="text" name="state"><br>
            City or town <br> <input type="text" name="city_or_town"><br>
            Steert and Number  <br> <input type="text" name="street_and_number"><br>
            Apartment No. <br> <input type="text" name="apartment_no"><br>
            Zip Code <br> <input type="text" name="zip_code"><br>
            
        <h2>Descrive How Injury Occured</h2>
            <input type="text" name="descrive_how_injury_occured">
            
  
        <h2>If Transportation Injury</h2>
            <h3>Specify:</h3>
                <input type="radio" name="transportation_injury">Driver / Operator
                <input type="radio" name="transportation_injury">Passenger
                <input type="radio" name="transportation_injury">Pedestrain
                <input type="radio" name="transportation_injury">Other (Specify): 
                <input type="text" name="transportation_injury_specify">
            
            
            
        <h2>Certifier</h2>
            <h3>Check Only One</h3>                      
                <input type="radio" name="certifier_check_only_one"> Certifing Physician - To the best of my knowledge, death occured due to the cause(s) and manner stated. <br>
                <input type="radio"   name="certifier_check_only_one"> Pronouncing and certifing physician - To the best of my knowledge, death occured at the time, date, and place and due to the cause(s) and manner stated. <br>
                <input type="radio"   name="certifier_check_only_one"> Medical Examiner or Coroner - On the basis of examination, and/or investigation, in my opinion, death occured at the time, date, and place and due to the cause(s) and manner stated. <br>
                    

            Signature to certifier: <br>                        <input type="text" name="signature_to_certifier"><br>            
            
            

        <h2>Person Completing cause of Death</h2>
            Name <br><input type="text" name="person_completing_cause_of_death_name"><br>
            Address <br><input type="text" name="person_completing_cause_of_death_address"><br>
            Zip Code <br><input type="text" name="person_completing_cause_of_death_zipcode"><br>
            Title of Certifier <br><input type="text" name="person_completing_cause_of_death_title_of_certifier"><br>
            License Number <br><input type="text" name="person_completing_cause_of_death_license_number"><br>
            Date of Certified <br><input type="text" name="person_completing_cause_of_death_date_of_certified">
            
<br>
	<input type="submit" value="submit">
</form>

</body>
</html>