<?php
include_once('lib/app.php');
//debug($_SESSION);
?>

<html>
<head>
    <link rel="stylesheet" href="style.css" type="text/css">
</head>
<body>


<center><h1>lab Exam 2</h1></center>




<fieldset><legend align="left">Student's Information</legend>



    <table border="2">

        <tr>
            <th>SL</th>
            <th>Student ID</th>
            <th>Student Name</th>
            <th>Email</th>
            <th>Date Of Birth</th>
            <th>Gender</th>
            <th>Job Type</th>
            <th>Action</th>
        </tr>
        <?php
        if(array_key_exists('death', $_SESSION)) {
            foreach ($_SESSION['death'] as $key => $value) {
                
                ?>
                <tr>
                    <td><?php echo $key; ?></td>
                    <td><?php
                        if(array_key_exists('s_id', $value) && !empty($value['s_id'])) {
                            echo $value['s_id'];
                        }
                        else{
                            echo "Not Provided";
                        }
                            ?>
                    </td>
                    <td><?php
                        if(array_key_exists('s_name', $value) && !empty($value['s_name'])) {
                            echo $value['s_name'];
                        }
                        ?>
                    </td>
                    <td><?php
                        if(array_key_exists('email', $value) && !empty($value['email'])) {
                            echo $value['email'];
                        }
                        ?>
                    </td>
                    <td><?php
                        if(array_key_exists('date_of_birth', $value) && !empty($value['date_of_birth'])) {
                            echo $value['date_of_birth'];
                        }
                        ?>
                    </td>
                    <td><?php
                        if(array_key_exists('gender1', $value) && !empty($value['gender1'])) {
                            echo $value['gender1'];
                        }
                        ?>
                    </td>
                    <td><?php
                        if(array_key_exists('job_status', $value) && !empty($value['job_status'])) {
                            echo $value['job_status'];
                        }
                        ?>
                    </td>

                    <td>
                        <a href="show.php?id=<?php echo $key; ?>">View</a> |
                        <a href="edit.php?id=<?php echo $key; ?>">Edit</a> |
                        <a href="delete.php?id=<?php echo $key; ?>">Delete</a>
                    </td>
                </tr>
            <?php
            }

        }
        else {
            ?>
            <tr>
                <td colspan="3">No Data Available</td>
            </tr>

        <?php
        }
        ?>

    <?php
   
?>
</table>
<a href="create.html">Back</a>
</body>
</html>