<html>
	<head>
        <link rel="stylesheet" href="style.css" type="text/css">
	</head>
	<body>
 
    		
			     <center><h1>Appendix A-Sample US Death Certificate Form</h1></center>
                    <p>The sample death reporting form including in this content profile reflects much of the data captured                        for the U.S. Standard Certificate of death. However, the VRDR Content profile may be modified to                            include and accommodate international death reporting requirements.</p>
			     <center><h1>Death Reporting for vital Records</h1></center>
		          <h2>Decendent's Name (include AKA's if any)</h2>
		          <form action="death_form" method="POST">

                      <fieldset><legend align="left">Decendent's Name</legend>

                            Last Name <br> 
                                <?php 
                                    if (array_key_exists('last_name',$_POST) && !empty( $_POST['last_name']))
                                        {
                                            echo "<b>";
                                            echo $_POST['last_name'];
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>";
                                            echo "not define";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                ?>
                                <br>
                            First Name  <br> 
                                <?php 
                                    if (array_key_exists('first_name',$_POST) && !empty( $_POST['first_name']))
                                        {
                                            echo "<b>";
                                            echo  $_POST['first_name'] ;
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>";
                                            echo "not define";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                ?>
                                <br>
                            Middle Name  <br> 
                                 <?php 
                                    if (array_key_exists('middle_name',$_POST) && !empty( $_POST['middle_name']))
                                        {
                                            echo "<b>";
                                            echo  $_POST['middle_name'] ;
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>";
                                            echo "not define";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                ?>                         
                          
                          <br>
                            
                            Date of birth  <br> 
                                 <?php 
                                    if (array_key_exists('date_of_birth',$_POST) && !empty( $_POST['date_of_birth']))
                                        {
                                            echo "<b>";
                                            echo  $_POST['date_of_birth'] ;
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>";
                                            echo "not define";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                ?>                           
                          
                          <br>
                            Gender  <br> 
                                 <?php 
                                    if (array_key_exists('gender1',$_POST) && !empty( $_POST['gender1']))
                                        {
                                            $selected_gender = $_POST['gender1'];
                                            echo "<b>";
                                            echo "You are ". $selected_gender;
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>";
                                            echo "not define";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                ?>                            
                          
                          
                          <br>
                            
                            Social Security Number  <br> 
                                 <?php 
                                    if (array_key_exists('social_security_number',$_POST) && !empty( $_POST['social_security_number']))
                                        {
                                            echo "<b>";
                                            echo  $_POST['social_security_number'] ;
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>";
                                            echo "not define";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                ?>                            
                          
                          <br>
                            Facility Name  <br> 
                          
                                 <?php 
                                    if (array_key_exists('facility_name',$_POST) && !empty( $_POST['facility_name']))
                                        {
                                            echo "<b>";
                                            echo  $_POST['facility_name'] ;
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>";
                                            echo "not define";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                ?>                            
                     </fieldset>

			<h2>Decendent's Hispanic Origin</h2>

                    <p><b>Check the box that best describes whether the decedent is Spanish / Hispanic / Latino. Check the "No" box if decendent is not Spanish / Hispanic / Latino :</b></p>
                <br>
                           
                          
                                 <?php 
                                   // $selected_hispanic_origin = $_POST['hispanic_origin'];

                                    if (array_key_exists('hispanic_origin',$_POST) && !empty( $_POST['hispanic_origin']))
                                        {

                                            $selected_hispanic_origin = $_POST['hispanic_origin'];
                                            echo "<b>";
                                            echo "You are ". $selected_hispanic_origin;
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }

                                    elseif (array_key_exists('hispanic_origin_specify',$_POST) && !empty( $_POST['hispanic_origin_specify']))
                                 {

                                     $selected_hispanic_origin_specify = $_POST['hispanic_origin_specify'];
                                     $spanish_specify = $_POST['spanish_specify'];
                                     echo "<b>";
                                     echo "You are ". $selected_hispanic_origin_specify;
                                     echo " ( ".$spanish_specify." )";
                                     echo "</b>";
                                     echo "<br>";
                                     echo "<br>";
                                 }

                                    else
                                        {
                                            echo "<b>";
                                            echo " Please select Hispanic Origin ";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }


                                   // echo "You are ". $selected_hispanic_origin;


                                    if (array_key_exists('spanish_specify',$_POST) && !empty( $_POST['spanish_specify']))
                                        {
                                            echo "<b>";
                                            echo " ".$_POST['spanish_specify'] ;
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>";
                                            echo " ";
                                            echo "</b>";
                                            echo "<br>";
                                            
                                        }

                                   // if (array_key_exists('spanish_specify',$_POST) && !empty( $_POST['spanish_specify']))
                                       // {
                                         //   echo "<b>";
                                         //   echo " ".$_POST['spanish_specify'] ;
                                          //  echo "</b>";
                                          //  echo "<br>";
                                           // echo "<br>";
                                       // }
                                   // else
                                      //  {
                                         //   echo "<b>";
                                          //  echo " ";
                                          //  echo "</b>";
                                          //  echo "<br>";
                                          //  echo "<br>";
                                      //  }

                                ?>                       
                      
                               

			<h2>Decendent's Race</h2>

			<p><b>Check one or more races to indicate what the Decendent considered herself to be:  </b></p>

                                 
                                <?php 
                                    if (array_key_exists('decedent_race',$_POST) && !empty( $_POST['decedent_race']))
                                        {

                                            $decendent_race = $_POST['decedent_race'];
                                            echo "<b>";
                                            echo "You are ". $decendent_race;
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>";
                                            echo " Please select Decendent's Race ";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                ?>

		<h2>Items Must be Completed by person Who Pronounces or Certificate Death.</h2>
                    <fieldset>  
                        <legend align="left">Death Certificate</legend>
                            Date Pronounced Dead <br> 
                                 <?php 
                                    if (array_key_exists('date_pronounced_dead',$_POST) && !empty( $_POST['date_pronounced_dead']))
                                        {
                                            echo "<b>";
                                            echo  $_POST['date_pronounced_dead'] ;
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>";
                                            echo "Not Define";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                ?>                          
                        
                            Time Pronounced Dead <br> 
                                <?php
                                    if (array_key_exists('time_pronounced_dead',$_POST) && !empty( $_POST['time_pronounced_dead']))
                                        {
                                            echo "<b>";
                                            echo $_POST['time_pronounced_dead'];
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>";
                                            echo "Not Define";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                            
                                        }
                                ?>
                        
                            Signature of Pronouncing Death <br> 
                                <?php
                                    if(array_key_exists('signature_of_pronouncing_death',$_POST) && !empty( $_POST['signature_of_pronouncing_death']))
                                        {
                                            echo "<b>";
                                            echo $_POST['signature_of_pronouncing_death'];
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else 
                                        {
                                            echo "<b>";
                                            echo "Not Define";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                ?>
                        
                            License Number <br>
                                <?php
                                    if(array_key_exists('license_number',$_POST) && !empty( $_POST['license_number']))
                                        {
                                            echo "<b>";
                                            echo $_POST['license_number'];
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else 
                                        {   
                                            echo "<b>";
                                            echo "Not Define";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                ?>
                        
                            Date Signed <br> 
                        
                                <?php
                                    if(array_key_exists('date_signed',$_POST) && !empty( $_POST['date_signed']))
                                        {
                                            echo "<b>";
                                            echo $_POST['date_signed'];
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else 
                                        {   
                                            echo "<b>";
                                            echo "Not Define";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                ?>                        
                            Actual Or Person Pronouncing Death <br>
                                <?php
                                    if(array_key_exists('Actual_Or_Person_Pronouncing_Death',$_POST) && !empty( $_POST['Actual_Or_Person_Pronouncing_Death']))
                                        {
                                            echo "<b>";
                                            echo $_POST['Actual_Or_Person_Pronouncing_Death'];
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>";
                                            echo "Not Define";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                            
                                ?>
                        
                            Actual Or Presumed time of Death <br> 
                                <?php
                                    if(array_key_exists('Actual_Or_Presumed_time_of_Death',$_POST) && !empty( $_POST['Actual_Or_Presumed_time_of_Death']))
                                        {
                                            echo "<b>";
                                            echo $_POST['Actual_Or_Presumed_time_of_Death'];
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>";
                                            echo "Not Define";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                            
                                ?>                            
                        
                            Was Medical Examiner Or Coroner Contacted <br> 
                                <?php 
                                    if (array_key_exists('Medical_Examiner',$_POST) && !empty( $_POST['Medical_Examiner']))
                                        {

                                            $medical_examiner = $_POST['Medical_Examiner'];
                                            echo "<b>";
                                            echo $medical_examiner;
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>";
                                            echo " Please select Medical Examiner Contacted ";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                ?>
                    </fieldset>


		<h2>Cause of Death ( See Instructions and examples)</h2>
            <fieldset>
                <legend>Cause of Death (Part - 1)</legend>
                    <p>PART 1. Enter the chain of events -- diseases, injuries, or complications -- that directly caused the                        death. DO NOT enter terminal events such as Approxmateinterval : cardiac arrest, respioratory arrest,                        or ventricular fibrillation without showing  the etiology. DO NOT ABBREVIATE. Enter only one cause                          ona line.<br> Ass Additional lines if necessary.</p>

                        a. 	Immediate Cause (Final diseases or condition resulting in death) <br> 
                                  
                                <?php
                                    if(array_key_exists('Immediate_Cause',$_POST) && !empty( $_POST['Immediate_Cause']))
                                        {
                                            echo "<b>";
                                            echo $_POST['Immediate_Cause'];
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>";
                                            echo "Not Define";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                            
                                ?> 
                
                            Due to (or as a consequence of): <br> 
                                  
                                 <?php
                                    if(array_key_exists('Immediate_Cause_Due_to',$_POST) && !empty( $_POST['Immediate_Cause_Due_to']))
                                        {
                                            echo "<b>";
                                            echo $_POST['Immediate_Cause_Due_to'];
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>";
                                            echo "Not Define";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                            
                                ?>               
                
                            Onset to death: <br> 
                                                                     
                                 <?php
                                    if(array_key_exists('Immediate_Cause_Onset_to_death',$_POST) && !empty( $_POST['Immediate_Cause_Onset_to_death']))
                                        {
                                            echo "<b>";
                                            echo $_POST['Immediate_Cause_Onset_to_death'];
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>";
                                            echo "Not Define";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                            
                                ?>                  
                
                        b. 	Sequentially list Conditions, (if any, leading to the cause listed on line a.) <br> 
                                                  
                                 <?php
                                    if(array_key_exists('Sequentially_list_Conditions',$_POST) && !empty( $_POST['Sequentially_list_Conditions']))
                                        {
                                            echo "<b>";
                                            echo $_POST['Sequentially_list_Conditions'];
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>";
                                            echo "Not Define";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                            
                                ?>                  
                            Due to (or as a consequence of): <br> 
                                                  
                                 <?php
                                    if(array_key_exists('Sequentially_list_Conditions_Due_to',$_POST) && !empty( $_POST['Sequentially_list_Conditions_Due_to']))
                                        {
                                            echo "<b>";
                                            echo $_POST['Sequentially_list_Conditions_Due_to'];
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>";
                                            echo "Not Define";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                            
                                ?>                 
                            Onset to death: <br> 
                                  
                                 <?php
                                    if(array_key_exists('Sequentially_list_Conditions_Onset_to_death',$_POST) && !empty( $_POST['Sequentially_list_Conditions_Onset_to_death']))
                                        {
                                            echo "<b>";
                                            echo $_POST['Sequentially_list_Conditions_Onset_to_death'];
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>";
                                            echo "Not Define";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                            
                                ?>                 
                                
                
                            Due to (or as a consequence of): <br> 
                                                  
                                 <?php
                                    if(array_key_exists('Sequentially_list_Conditions_Due_to',$_POST) && !empty( $_POST['Sequentially_list_Conditions_Due_to']))
                                        {
                                            echo "<b>";
                                            echo $_POST['Sequentially_list_Conditions_Due_to'];
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>";
                                            echo "Not Define";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                            
                                ?>                 
                
                        c. 	Enter the Underlying Cause, (Diseases or injury that initiated the events resulting in death)                           <br>
                                                 
                                 <?php
                                    if(array_key_exists('Enter_the_Underlying_Cause',$_POST) && !empty( $_POST['Enter_the_Underlying_Cause']))
                                        {
                                            echo "<b>";
                                            echo $_POST['Enter_the_Underlying_Cause'];
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>";
                                            echo "Not Define";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                            
                                ?>                 
                            Due to (or as a consequence of): <br> 
                                                
                                 <?php
                                    if(array_key_exists('Enter_the_Underlying_Cause_Due_to',$_POST) && !empty( $_POST['Enter_the_Underlying_Cause_Due_to']))
                                        {
                                            echo "<b>";
                                            echo $_POST['Enter_the_Underlying_Cause_Due_to'];
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>";
                                            echo "Not Define";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                            
                                ?>                 
                            Onset to death: <br> 
                                                  
                                 <?php
                                    if(array_key_exists('Enter_the_Underlying_Cause_Onset_to_death',$_POST) && !empty( $_POST['Enter_the_Underlying_Cause_Onset_to_death']))
                                        {
                                            echo "<b>";
                                            echo $_POST['Enter_the_Underlying_Cause_Onset_to_death'];
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>";
                                            echo "Not Define";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                            
                                ?>                 
                        d. 	Last <br>
                                  
                                 <?php
                                    if(array_key_exists('last_cause_of_death',$_POST) && !empty( $_POST['last_cause_of_death']))
                                        {
                                            echo "<b>";
                                            echo $_POST['last_cause_of_death'];
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>";
                                            echo "Not Define";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                            
                                ?>                  
                
                            Onset to death: <br> 
                                 
                                 <?php
                                    if(array_key_exists('Enter_the_Underlying_Cause_Onset_to_death',$_POST) && !empty( $_POST['Enter_the_Underlying_Cause_Onset_to_death']))
                                        {
                                            echo "<b>";
                                            echo $_POST['Enter_the_Underlying_Cause_Onset_to_death'];
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>";
                                            echo "Not Define";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                            
                                ?>                
                
            </fieldset>
                      
		<p><b>PART 2.</b> Enter other significent conditions contributing to death but not resulting in the underlying cause given in PART 1. </p>
                <fieldset>  
                    <legend>Cause of Death (Part - 2)</legend>
                            
                    
                                 <?php
                                    if(array_key_exists('Enter_other_significent_conditions',$_POST) && !empty( $_POST['Enter_other_significent_conditions']))
                                        {
                                            echo "<b>";
                                            echo $_POST['Enter_other_significent_conditions'];
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo " ";
                                            echo "<br>";
                                            echo "<br>";
                                            
                                        }
                            
                                ?>                     
                            Was An Autospy performed? <br>
                    
                                <?php 
                                    if (array_key_exists('Autospy_performed',$_POST) && !empty( $_POST['Autospy_performed']))
                                        {

                                            $autospy_performed = $_POST['Autospy_performed'];
                                            echo "<b>";
                                            echo $autospy_performed;
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>";
                                            echo " Please select Autospy Performed ";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                ?>                    
                            <br>
                            Were Autospy Finding Available to complete the cause of death?<br>
                    
                                <?php 
                                    if (array_key_exists('complete_the_cause_of_death',$_POST) && !empty( $_POST['complete_the_cause_of_death']))
                                        {

                                            $complete_the_cause_of_death = $_POST['complete_the_cause_of_death'];
                                            echo "<b>";
                                            echo $complete_the_cause_of_death;
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>";
                                            echo " Please select Autospy Finding Available to complete the cause of death";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                ?>                      

                            <br>
                            Did Tobaco use contribute to death?<br>
                            

                                <?php 
                                    if (array_key_exists('tobaco_use_contribute_to_death',$_POST) && !empty( $_POST['tobaco_use_contribute_to_death']))
                                        {

                                            $tobaco_use_contribute_to_death = $_POST['tobaco_use_contribute_to_death'];
                                            echo "<b>";
                                            echo $tobaco_use_contribute_to_death;
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>";
                                            echo " Please select Autospy Finding Available to complete the cause of death";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                ?>                     
                    
                </fieldset>
            
        <h2>If female:</h2>
                    <fieldset>  
                        <legend>If Female Please fill up</legend>
                            
                                 <?php 
                                    if (array_key_exists('if_female',$_POST) && !empty( $_POST['if_female']))
                                        {

                                            $if_female = $_POST['if_female'];
                                            echo "<b>";
                                            echo $if_female;
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>";
                                            echo " Please select If Female Option";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                ?> 
                      
                      </fieldset>
            
                <h2>Manner of death</h2> 
                    <fieldset>
                        <legend>Manner of Death</legend>
                           
                                 <?php 
                                    if (array_key_exists('manner_of_death',$_POST) && !empty( $_POST['manner_of_death']))
                                        {

                                            $manner_of_death = $_POST['manner_of_death'];
                                            echo "<b>";
                                            echo $manner_of_death;
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>";
                                            echo " Please select Manner of Death";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                ?> 
                        
                    </fieldset>
            
                      
                 <h2>Injury</h2>
                    <fieldset>  
                        <legend>Injury</legend>
                                                             
                                 <?php
                                    if(array_key_exists('date_of_injury',$_POST) && !empty( $_POST['date_of_injury']))
                                        {
                                            echo "<b>";
                                            echo $_POST['date_of_injury'];
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>";
                                            echo "Not Define";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                            
                                ?>                        
                        
                            Time of Injury <br> 
                                                             
                                 <?php
                                    if(array_key_exists('time_of_injury',$_POST) && !empty( $_POST['time_of_injury']))
                                        {
                                            echo "<b>";
                                            echo $_POST['time_of_injury'];
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>";
                                            echo "Not Define";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                            
                                ?>                          
                        
                            Palace of Injury <br> 
                                                                              
                                 <?php
                                    if(array_key_exists('palace_of_injury',$_POST) && !empty( $_POST['palace_of_injury']))
                                        {
                                            echo "<b>";
                                            echo $_POST['palace_of_injury'];
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>";
                                            echo "Not Define";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                            
                                ?>                         
                            Injury at work <br>
                            
                                 <?php 
                                    if (array_key_exists('injury_at_work',$_POST) && !empty( $_POST['injury_at_work']))
                                        {

                                            $injury_at_work = $_POST['injury_at_work'];
                                            echo "<b>";
                                            echo $injury_at_work;
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>";
                                            echo " Please select Enjory at Work";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    ?>
                        
                        
                    </fieldset>
            
            
            <h2>Location of Injury</h2>  
                <fieldset>
                    <legend>Location Of Injury</legend>
                            state <br>
                                                                              
                                 <?php
                                    if(array_key_exists('state',$_POST) && !empty( $_POST['state']))
                                        {
                                            echo "<b>";
                                            echo $_POST['state'];
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>";
                                            echo "Not Define";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                            
                                ?>                     
                    
                            City or town <br> 
                                                                                                  
                                 <?php
                                    if(array_key_exists('city_or_town',$_POST) && !empty( $_POST['city_or_town']))
                                        {
                                            echo "<b>";
                                            echo $_POST['city_or_town'];
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>";
                                            echo "Not Define";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                            
                                ?>                    
                            Steert and Number  <br> 
                    
                                 <?php
                                    if(array_key_exists('street_and_number',$_POST) && !empty( $_POST['street_and_number']))
                                        {
                                            echo "<b>";
                                            echo $_POST['street_and_number'];
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>";
                                            echo "Not Define";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                            
                                ?>                     
                            Apartment No. <br> 
                    
                    
                                 <?php
                                    if(array_key_exists('apartment_no',$_POST) && !empty( $_POST['apartment_no']))
                                        {
                                            echo "<b>";
                                            echo $_POST['apartment_no'];
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>";
                                            echo "Not Define";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                            
                                ?>                     
                            Zip Code <br>
                    
                    
                                 <?php
                                    if(array_key_exists('zip_code',$_POST) && !empty( $_POST['zip_code']))
                                        {
                                            echo "<b>";
                                            echo $_POST['zip_code'];
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>";
                                            echo "Not Define";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                            
                                ?>                      
                </fieldset>
            
            <h2>Descrive How Injury Occured</h2>
                                      
                    
                                 <?php
                                    if(array_key_exists('descrive_how_injury_occured',$_POST) && !empty( $_POST['descrive_how_injury_occured']))
                                        {
                                            echo "<b>";
                                            echo $_POST['descrive_how_injury_occured'];
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>";
                                            echo "Not Define";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                            
                                ?>                      
            
  
            <h2>If Transportation Injury</h2>
                <fieldset> 
                    <legend>Transportain Injury</legend>
                            <h3>Specify:</h3>
                                
                                 <?php 
                                    if (array_key_exists('transportation_injury',$_POST) && !empty( $_POST['transportation_injury']))
                                        {

                                            $transportation_injury = $_POST['transportation_injury'];
                                            echo "<b>";
                                            echo $transportation_injury;
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>";
                                            echo " Please select Transportation Injury Option";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    ?>
                                
                    
                                 <?php
                                    if(array_key_exists('transportation_injury_specify',$_POST) && !empty( $_POST['transportation_injury_specify']))
                                        {
                                            echo "<b>";
                                            echo $_POST['transportation_injury_specify'];
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo " ";
                                            echo "<br>";
                                            
                                        }
                            
                                ?> 
                    
                </fieldset>
            
            
            
            <h2>Certifier</h2>
                <fieldset>
                          <legend>Certifier</legend>


                                 <?php 
                                    if (array_key_exists('certifier_check_only_one',$_POST) && !empty( $_POST['certifier_check_only_one']))
                                        {

                                            $certifier_check_only_one = $_POST['certifier_check_only_one'];
                                            echo "<b>";
                                            echo $certifier_check_only_one;
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>";
                                            echo " Please select Certifier Option ";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    ?>                          


                        Signature to certifier: <br>                        
                                       
                                  <?php
                                    if(array_key_exists('signature_to_certifier',$_POST) && !empty( $_POST['signature_to_certifier']))
                                        {
                                            echo "<b>";
                                            echo $_POST['signature_to_certifier'];
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>";
                                            echo "Please added Certifier signature";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                            
                                ?>           
                    
                      </fieldset>

                <h2>Person Completing cause of Death</h2>
                    <fieldset>  
                        <legend>Completing cause of death</legend>
                            Name : <br>
                                       
                                  <?php
                                    if(array_key_exists('person_completing_cause_of_death_name',$_POST) && !empty( $_POST['person_completing_cause_of_death_name']))
                                        {
                                            echo "<b>";
                                            echo $_POST['person_completing_cause_of_death_name'];
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>"; 
                                            echo "Not Define";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                            
                                ?>                          
                        
                        
                            Address : <br>
                        
                                  <?php
                                    if(array_key_exists('person_completing_cause_of_death_address',$_POST) && !empty( $_POST['person_completing_cause_of_death_address']))
                                        {
                                            echo "<b>";
                                            echo $_POST['person_completing_cause_of_death_address'];
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>"; 
                                            echo "Not Define";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                            
                                ?>                          
                            Zip Code : <br>
                        
                                  <?php
                                    if(array_key_exists('person_completing_cause_of_death_address',$_POST) && !empty( $_POST['person_completing_cause_of_death_zipcode']))
                                        {
                                            echo "<b>";
                                            echo $_POST['person_completing_cause_of_death_zipcode'];
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>";
                                            echo "Not Define";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                            
                                ?>                        
                        
                            Title of Certifier : <br>
                                  <?php
                                    if(array_key_exists('person_completing_cause_of_death_title_of_certifier',$_POST) && !empty( $_POST['person_completing_cause_of_death_title_of_certifier']))
                                        {
                                            echo "<b>";
                                            echo $_POST['person_completing_cause_of_death_title_of_certifier'];
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>"; 
                                            echo "Not Define";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                            
                                ?>                        
                                                
                        
                            License Number :<br>
                        
                                  <?php
                                    if(array_key_exists('person_completing_cause_of_death_license_number',$_POST) && !empty( $_POST['person_completing_cause_of_death_license_number']))
                                        {
                                            echo "<b>";
                                            echo $_POST['person_completing_cause_of_death_license_number'];
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>"; 
                                            echo "Not Define";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                            
                                ?>                         
                        
                            Date of Certified :<br>
                        
                                  <?php
                                    if(array_key_exists('person_completing_cause_of_death_date_of_certified',$_POST) && !empty( $_POST['person_completing_cause_of_death_date_of_certified']))
                                        {
                                            echo "<b>";
                                            echo $_POST['person_completing_cause_of_death_date_of_certified'];
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                                    else
                                        {
                                            echo "<b>";
                                            echo "Not Define";
                                            echo "</b>";
                                            echo "<br>";
                                            echo "<br>";
                                        }
                            
                                ?>                        
                        
                    </fieldset>
            


</body>
</html>