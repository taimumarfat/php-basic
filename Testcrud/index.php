<?php
include_once('lib/application.php');
$data = findAll();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo PAGE_TITLE;?></title>
    </head>
    <body>
        <div><?php echo $_SESSION['message'];?></div>
        <p>|Search| Download as PDF | XL</p>
        <p>
            
        <a href="add.php">Click here</a> to add new email.
        <div>
            <?php 
        
        if(array_key_exists('message', $_SESSION) && !empty($_SESSION['message'])){
            echo $_SESSION['message']; 
        }
        ?>
        </div>
        </p>
        <table border="1">
            <tr>
                <th>Sl</th>
                <th>Email</th>
                <th>Name</th>
                <th>Gender</th>
                <th>Action</th>
            </tr>
            <?php 
            if(isset($data) && !empty($data)){
           
            
            foreach($data as $key=>$value){ 

            ?>
            <tr>
                <td><?php echo $key; ?></td>
                <td><?php echo $value['email'];?></td>
                 <td><?php echo $value['name'];?></td>
                 <td><?php echo $value['gender'];?></td>
                <td>
                    <a href="show.php?id=<?php echo $key;?>">View</a>
                    <a href="edit.php?id=<?php echo $key;?>">Edit</a>
                    <a href="delete.php?id=<?php echo $key;?>">Delete</a>
                
                </td>
            </tr>
            <?php 
            
            } 
                 
            }else{
                ?>
            <tr>
                <td colspan="3">
                    <p>
                    No data available.<a href="add.php.html">Click here</a> to add new email.
                    </p>
                </td>
            </tr>
            <?php
            }
            
            ?>
        </table>
        |Paging|
    </body>
</html>
