<?php
include_once('lib/app.php');
//debug($_GET['id']);
$view=$_SESSION['death'][$_GET['id']];
?>
<html>
<head>
    <link rel="stylesheet" href="style.css" type="text/css">
</head>
<body>

<h1>Your Details</h1>
<dl>
    <dt>Last Name</dt>
    <dd><?php
        if(array_key_exists('last_name',$view) && !empty($view['last_name'])) {
            echo $view['last_name'];

        }
        else
        {
            echo "Not Provided";
        }
        ?>
    </dd>

    <dt>First Name</dt>
    <dd><?php
        if(array_key_exists('first_name',$view) && !empty($view['first_name'])) {
            echo $view['first_name'];

        }
        else
        {
            echo "Not Provided";
        }
        ?>
    </dd>
    <dt>Middle Name</dt>
    <dd><?php
        if(array_key_exists('middle_name',$view) && !empty($view['middle_name'])) {
            echo $view['middle_name'];

        }
        else
        {
            echo "Not Provided";
        }
        ?>
    </dd>
    <dt>Date of Birth</dt>
    <dd><?php
        if(array_key_exists('date_of_birth',$view) && !empty($view['date_of_birth'])) {
            echo $view['date_of_birth'];

        }
        else
        {
            echo "Not Provided";
        }
        ?>
    </dd>
    <dt>Gender</dt>
    <dd><?php
        if(array_key_exists('gender1',$view) && !empty($view['gender1'])) {
            echo $view['gender1'];

        }
        else
        {
            echo "Not Provided";
        }
        ?>
    </dd>
    <dt>Social Security Number</dt>
    <dd><?php
        if(array_key_exists('social_security_number',$view) && !empty($view['social_security_number'])) {
            echo $view['social_security_number'];

        }
        else
        {
            echo "Not Provided";
        }
        ?>
    </dd>
    <dt>Facility Name</dt>
    <dd><?php
        if(array_key_exists('facility_name',$view) && !empty($view['facility_name'])) {
            echo $view['facility_name'];

        }
        else
        {
            echo "Not Provided";
        }
        ?>
    </dd>
    <dt>Hispanic Origin</dt>
    <dd><?php
        if(array_key_exists('hispanic_origin',$view) && !empty($view['hispanic_origin'])) {
            echo $view['hispanic_origin'];

        }
        else
        {
            echo "Not Provided";
        }
        ?>
    </dd>
</dl>

<nav>
    <li><a href="index_1.php">Back</li>
    <li><a href="add.php">Create</li>
    <li><a href="#">Back</li>


</nav>
</body>
</html>