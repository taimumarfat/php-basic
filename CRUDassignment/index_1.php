<?php
include_once('lib/app.php');
//debug($_SESSION);
?>

<html>
<head>
    <link rel="stylesheet" href="style.css" type="text/css">
</head>
<body>


<center><h1>Appendix A-Sample US Death Certificate Form</h1></center>
<p>The sample death reporting form including in this content profile reflects much of the data captured                        for the U.S. Standard Certificate of death. However, the VRDR Content profile may be modified to                            include and accommodate international death reporting requirements.</p>
<center><h1>Death Reporting for vital Records</h1></center>
<h2>Decendent's Name (include AKA's if any)</h2>


<fieldset><legend align="left">Decendent's Name</legend>



    <table border="2">

        <tr>
            <th>ID</th>
            <th>Last Name</th>
            <th>First Name</th>
            <th>Middle Name</th>
            <th>Date of Birth</th>
            <th>Gender</th>
            <th>Social Security Number</th>
            <th>Facility Name</th>
            <th>Hispanic Origin</th>
            <th>Action</th>
        </tr>
        <?php
        if(array_key_exists('death', $_SESSION)) {
            foreach ($_SESSION['death'] as $key => $value) {
                //echo $key . "--" . $value . "<br>";
                ?>
                <tr>
                    <td><?php echo $key; ?></td>
                    <td><?php
                        if(array_key_exists('last_name', $value) && !empty($value['last_name'])) {
                            echo $value['last_name'];
                        }
                        else{
                            echo "Not Provided";
                        }
                            ?>
                    </td>
                    <td><?php
                        if(array_key_exists('first_name', $value) && !empty($value['first_name'])) {
                            echo $value['first_name'];
                        }
                        else
                        {
                            echo "Not Provided";
                        }
                        ?>
                    </td>
                    <td><?php
                        if(array_key_exists('middle_name', $value) && !empty($value['middle_name'])) {
                            echo $value['middle_name'];
                        }
                        else
                        {
                            echo "Not Provided";
                        }
                        ?>
                    </td>
                    <td><?php
                        if(array_key_exists('date_of_birth', $value) && !empty($value['date_of_birth'])) {
                            echo $value['date_of_birth'];
                        }
                        else
                        {
                            echo "Not Provided";
                        }
                        ?>
                    </td>
                    <td><?php
                        if(array_key_exists('gender1', $value) && !empty($value['gender1'])) {
                            echo $value['gender1'];
                        }
                        else
                        {
                            echo "Not Provided";
                        }
                        ?>
                    </td>
                    <td><?php
                        if(array_key_exists('social_security_number', $value) && !empty($value['social_security_number'])) {
                            echo $value['social_security_number'];
                        }
                        else
                        {
                            echo "Not Provided";
                        }
                        ?>
                    </td>
                    <td><?php
                        if(array_key_exists('facility_name', $value) && !empty($value['facility_name'])) {
                            echo $value['facility_name'];
                        }
                        else
                        {
                            echo "Not Provided";
                        }
                        ?>
                    </td>
                    <td><?php
                        if(array_key_exists('hispanic_origin', $value) && !empty($value['hispanic_origin'])) {
                            echo $value['hispanic_origin'];
                        }
                        else
                        {
                            echo "Not Provided";
                        }
                        ?>
                    </td>

                    <td>
                        <a href="show_1.php?id=<?php echo $key; ?>">View</a> |
                        <a href="edit_1.php?id=<?php echo $key; ?>">Edit</a> |
                        <a href="delete_1.php?id=<?php echo $key; ?>">Delete</a>
                    </td>
                </tr>
            <?php
            }

        }
        else {
            ?>
            <tr>
                <td colspan="3">No Data Available</td>
            </tr>

        <?php
        }
        ?>

    <?php
    /**


    Social Security Number  <br> <input type="text" name="social_security_number" size="23"><br>
    Facility Name  <br> <input type="text" name="facility_name" size="45">
</fieldset>

<h2>Decendent's Hispanic Origin</h2>


<p><b>Check the box that best describes whether the decedent is Spanish / Hispanic / Latino. Check the "No" box if decendent is not Spanish / Hispanic / Latino :</b></p>
<br>
<input type="radio" name="hispanic_origin" value="not Spanish/Hispanic/Latino">No, Not Spanish/Hispanic/Latino
<input type="radio" name="hispanic_origin" value="Mexican, Mexican American, Chicano">Yes, Mexican, Mexican American, Chicano
<input type="radio" name="hispanic_origin" value="Puerto Rican">Yes, Puerto Rican
<input type="radio" name="hispanic_origin" value="Cuban">Yes, Cuban


<input type="radio" name="hispanic_origin" value="Other">Yes, Other  Spanish/Hispanic/Latino(Specify):<br>
<input type="text" name="spanish_specify" size="100" id="spanish_specify">


<br>
<br>


<h2>Decendent's Race</h2>

<p><b>Check one or more races to indicate what the Decendent considered herself to be:  </b></p>

<input type="radio" name="decedent_race" value="White">White
<input type="radio" name="decedent_race" value="Filipino">Filipino
<input type="radio" name="decedent_race" value="Native Hawaiian">Native Hawaiian
<input type="radio" name="decedent_race" value="Black or African American">Black or African American <br>
<input type="radio" name="decedent_race" value="Japanese">Japanese
<input type="radio" name="decedent_race" value="Guamanian or Chamorro">Guamanian or Chamorro
<input type="radio" name="decedent_race" value="American Indian or Alaska Native (Name of the enrolled or principal Tribe)">American Indian or Alaska Native (Name of the enrolled or principal Tribe) <input type="text" name="alaska_native">
<input type="radio" name="decedent_race" value="Vietnamese">Vietnamese<br>
<input type="radio" name="decedent_race" value="Other Pacific Islander (Specify)">Other Pacific Islander (Specify) <input type="text" name="other_pacific">
<input type="radio" name="decedent_race" value="Asian Indian">Asian Indian
<input type="radio" name="decedent_race" value="Other Asian (specify)">Other Asian (specify) <input type="text" name="other_asian">
<input type="radio" name="decedent_race" value="Other (Specify)">Other (Specify) <input type="text" name="otehr_specify"> <br>
<input type="radio" name="decedent_race" value="Chinese">Chinese
<input type="radio" name="decedent_race" value="Korean">Korean
<input type="radio" name="decedent_race" value="Samoan">Samoan

<h2>Items Must be Completed by person Who Pronounces or Certificate Death.</h2>
<fieldset>
    <legend align="left">Death Certificate</legend>
    Date Pronounced Dead <br> <input type="date" name="date_pronounced_dead"><br>
    Time Pronounced Dead <br> <input type="text" name="time_pronounced_dead"><br>
    Signature of Pronouncing Death <br> <input type="text" name="signature_of_pronouncing_death"><br>
    License Number <br> <input type="text" name="license_number"><br>
    Date Signed <br> <input type="date" name="date_signed"><br>
    Actual Or Person Pronouncing Death <br> <input type="text" name="Actual_Or_Person_Pronouncing_Death"><br>
    Actual Or Presumed time of Death <br> <input type="text" name="Actual_Or_Presumed_time_of_Death"><br>
    Was Medical Examiner Or Coroner Contacted <br>
    <input type="radio" name="Medical_Examiner" value="Yes" >Yes
    <input type="radio" name="Medical_Examiner" value="No">No
</fieldset>

<h2>Cause of Death ( See Instructions and examples)</h2>
<fieldset>
    <legend>Cause of Death (Part - 1)</legend>
    <p>PART 1. Enter the chain of events -- diseases, injuries, or complications -- that directly caused the                        death. DO NOT enter terminal events such as Approxmateinterval : cardiac arrest, respioratory arrest,                        or ventricular fibrillation without showing  the etiology. DO NOT ABBREVIATE. Enter only one cause                          ona line.<br> Ass Additional lines if necessary.</p>

    a. 	Immediate Cause (Final diseases or condition resulting in death) <br>
    <input type="text" name="Immediate_Cause"><br>
    Due to (or as a consequence of): <br>
    <input type="text" name="Immediate_Cause_Due_to"><br>
    Onset to death: <br>
    <input type="text" name="Immediate_Cause_Onset_to_death"><br>
    b. 	Sequentially list Conditions, (if any, leading to the cause listed on line a.) <br>
    <input type="text" name="Sequentially_list_Conditions"><br>
    Due to (or as a consequence of): <br>
    <input type="text" name="Sequentially_list_Conditions_Due_to"><br>
    Onset to death: <br>
    <input type="text" name="Sequentially_list_Conditions_Onset_to_death"><br>
    c. 	Enter the Underlying Cause, (Diseases or injury that initiated the events resulting in death)                           <br>
    <input type="text" name="Enter_the_Underlying_Cause"><br>
    Due to (or as a consequence of): <br>
    <input type="text" name="Enter_the_Underlying_Cause_Due_to"><br>
    Onset to death: <br>
    <input type="text" name="Enter_the_Underlying_Cause_Onset_to_death"><br>
    d. 	Last <br>
    <input type="text" name="last_cause_of_death"><br>
    Onset to death: <br>
    <input type="text" name="Enter_the_Underlying_Cause_Onset_to_death"><br>
</fieldset>

<p><b>PART 2.</b> Enter other significent conditions contributing to death but not resulting in the underlying cause given in PART 1. </p>
<fieldset>
    <legend>Cause of Death (Part - 2)</legend>
    <textarea name="Enter_other_significent_conditions" rows="3" cols="70"></textarea><br>
    Was An Autospy performed? <br>
    <input type="radio" name="Autospy_performed" value="Yes">Yes
    <input type="radio" name="Autospy_performed" value="No">No
    <br>
    Were Autospy Finding Available to complete the cause of death? <br>
    <input type="radio" name="complete_the_cause_of_death" value="yes">Yes
    <input type="radio" name="complete_the_cause_of_death" value="No">No

    <br>
    Did Tobaco use contribute to death? <br>
    <input type="radio" name="tobaco_use_contribute_to_death" Value="Yes">Yes
    <input type="radio" name="tobaco_use_contribute_to_death" value="No">No
    <input type="radio" name="tobaco_use_contribute_to_death" value="Probably">Probably
    <input type="radio" name="tobaco_use_contribute_to_death" value="Unknown">Unknown
</fieldset>
<h2>If female:</h2>
<fieldset>
    <legend>If Female Please fill up</legend>
    <input type="radio" name="if_female" value="Not Pregnent within past year">Not Pregnent within past year
    <input type="radio" name="if_female" value="Pregnent at time of death">Pregnent at time of death
    <input type="radio" name="if_female" value="Not Pregnent, but pregnent within 42 days of death">Not Pregnent, but pregnent within 42 days of death
    <input type="radio" name="if_female" value="Not Pregnent, but pregnent 43 days to 1 year before death">Not Pregnent, but pregnent 43 days to 1 year before death
    <input type="radio" name="if_female" value="Unknown if pregnent within the past year">Unknown if pregnent within the past year
</fieldset>

<h2>Manner of death</h2>
<fieldset>
    <legend>Manner of Death</legend>
    <input type="radio" name="manner_of_death" value="Natural">Natural
    <input type="radio" name="manner_of_death" value="Homicide">Homicide
    <input type="radio" name="manner_of_death" value="Accident">Accident
    <input type="radio" name="manner_of_death" value="Pending Investigation ">Pending Investigation
    <input type="radio" name="manner_of_death" value="Sucide">Sucide
    <input type="radio" name="manner_of_death" value="Could not be determined">Could not be determined
</fieldset>
<h2>Injury</h2>
<fieldset>
    <legend>Injury</legend>
    Date of Injury <br> <input type="date" name="date_of_injury"><br>
    Time of Injury <br> <input type="text" name="time_of_injury"><br>
    Palace of Injury <br> <input type="text" name="palace_of_injury"><br>
    Injury at work
    <input type="radio" name="injury_at_work" value="Yes">Yes
    <input type="radio" name="injury_at_work" value="No">No
</fieldset>

<h2>Location of Injury</h2>
<fieldset>
    <legend>Location Of Injury</legend>
    state <br> <input type="text" name="state"><br>
    City or town <br> <input type="text" name="city_or_town"><br>
    Steert and Number  <br> <input type="text" name="street_and_number"><br>
    Apartment No. <br> <input type="text" name="apartment_no"><br>
    Zip Code <br> <input type="text" name="zip_code"><br>
</fieldset>
<h2>Descrive How Injury Occured</h2>
<input type="text" name="descrive_how_injury_occured" size="100">


<h2>If Transportation Injury</h2>
<fieldset>
    <legend>Transportain Injury</legend>
    <h3>Specify:</h3>
    <input type="radio" name="transportation_injury" value="Driver / Operator">Driver / Operator
    <input type="radio" name="transportation_injury" value="Passenger">Passenger
    <input type="radio" name="transportation_injury" value="Pedestrain">Pedestrain
    <input type="radio" name="transportation_injury" value="Other (Specify):">Other (Specify):
    <input type="text" name="transportation_injury_specify" >

</fieldset>

<h2>Certifier</h2>
<h3>Check Only One</h3>
<fieldset>
    <legend>Certifier</legend>
    <input type="radio" name="certifier_check_only_one" value="Certifing Physician - To the best of my knowledge, death occured due to the cause(s) and manner stated."> Certifing Physician - To the best of my knowledge, death occured due to the cause(s) and manner stated. <br>
    <input type="radio"   name="certifier_check_only_one" value="Pronouncing and certifing physician - To the best of my knowledge, death occured at the time, date, and place and due to the cause(s) and manner stated."> Pronouncing and certifing physician - To the best of my knowledge, death occured at the time, date, and place and due to the cause(s) and manner stated. <br>
    <input type="radio"   name="certifier_check_only_one" value="Medical Examiner or Coroner - On the basis of examination, and/or investigation, in my opinion, death occured at the time, date, and place and due to the cause(s) and manner stated."> Medical Examiner or Coroner - On the basis of examination, and/or investigation, in my opinion, death occured at the time, date, and place and due to the cause(s) and manner stated. <br>


    Signature to certifier: <br>
    <input type="text" name="signature_to_certifier"><br>

</fieldset>

<h2>Person Completing cause of Death</h2>
<fieldset>
    <legend>Completing cause of death</legend>
    Name <br><input type="text" name="person_completing_cause_of_death_name"><br>
    Address <br><input type="text" name="person_completing_cause_of_death_address"><br>
    Zip Code <br><input type="text" name="person_completing_cause_of_death_zipcode"><br>
    Title of Certifier <br><input type="text" name="person_completing_cause_of_death_title_of_certifier"><br>
    License Number <br><input type="text" name="person_completing_cause_of_death_license_number"><br>
    Date of Certified <br><input type="text" name="person_completing_cause_of_death_date_of_certified">
</fieldset>
<br>
    **/
?>
        </table>

<!--
<input type="submit" value="submit">
-->



<a href="add.php">Back</a>
</body>
</html>